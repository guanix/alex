Template.configureLoginServiceDialogForMediawiki.helpers({
  siteUrl: function () {
    return Meteor.absoluteUrl();
  }
});

Template.configureLoginServiceDialogForMediawiki.fields = function () {
  return [
    {property: 'wikiUrl', label: 'Wiki URL'},
    {property: 'clientId', label: 'Key'},
    {property: 'secret', label: 'Secret'}
  ];
};
