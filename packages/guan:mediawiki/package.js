Package.describe({
  name: 'guan:mediawiki',
  summary: 'MediaWiki OAuth flow',
  version: '1.0.0'
});

Package.onUse(function (api) {
  api.use('oauth1', ['server']);
  api.use('oauth', ['client', 'server']);
  api.use('templating', 'client');
  api.use('random', 'client');
  api.use('mongo', ['client', 'server']);
  api.use('service-configuration', ['client', 'server']);

  api.export('Mediawiki');

  api.addFiles(
    ['mediawiki_configure.html',
     'mediawiki_configure.js',
     'mediawiki_client.js'
     ],
    'client');

  api.addFiles('mediawiki_server.js', 'server');
});
