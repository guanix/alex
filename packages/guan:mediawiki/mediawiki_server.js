Mediawiki = {};

//MediawikiTokens = new Mongo.Collection("mediawiki_tokens");

//Meteor.startup(function () {
//  if (MediawikiTokens.find().count() == 0) {
//    MediawikiTokens._ensureIndex({createdAt: 1}, {expireAfterSeconds: 3600});
//  }
//});

var config = ServiceConfiguration.configurations.findOne({service: 'mediawiki'});
if (!config) {
  throw new ServiceConfiguration.ConfigError('mediawiki');
}

var urls = {
  authenticate: config.wikiUrl + '?title=' +
    encodeURIComponent("Special:OAuth/authorize") +
    '&oauth_consumer_key=' + config.clientId
};

OAuth.registerService('mediawiki', 1, urls, function(query) {
  console.log("oauth query");
  return {};
});

OAuth1Binding.prototype.prepareRequestToken = function (callbackUrl) {
  var self = this;

  self._config.consumerKey = config.clientId;
  self._config.secret = config.secret;

  var headers = self._buildHeader({
    format: 'json',
    oauth_callback: 'oob'
  });

  var initiateUrl = config.wikiUrl + '?format=json&oauth_callback=oob&title=Special%3AOAuth%2Finitiate';
  var response = self._call('GET', initiateUrl, headers);
  var r = JSON.parse(response.content);
  if (!r.oauth_callback_confirmed) {
    throw _.extend(new Error("oauth_callback_confirmed false when requesting oauth1 token"),
      {response: response});
  }

  self.requestToken = r.key;
  self.requestTokenSecret = r.secret;

//  MediawikiTokens.insert({
//    _id: r.key,
//    secret: r.secret,
//    createdAt: new Date()
//  });
};
