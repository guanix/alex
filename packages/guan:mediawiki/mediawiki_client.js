Mediawiki = {};

//var config = ServiceConfiguration.configurations.findOne({service: 'mediawiki'});
//Mediawiki.urls = {
//  authenticate: config.wikiUrl + '?title=Special:OAuth/authorize'
//};

Mediawiki.requestCredential = function (options, credentialRequestCompleteCallback) {
  console.log("Mediawiki.requestCredential");

  if (!credentialRequestCompleteCallback && typeof options === 'function') {
    credentialRequestCompleteCallback = options;
    options = {};
  }

  var config = ServiceConfiguration.configurations.findOne({service: 'mediawiki'});
  console.log("config", config);
  if (!config) {
    credentialRequestCompleteCallback && credentialRequestCompleteCallback(
      new ServiceConfiguration.ConfigError());
      return;
  }

  var credentialToken = Random.secret();
  console.log('credentialToken', credentialToken);
  var loginStyle = OAuth._loginStyle('mediawiki', config, options);
  var loginPath = '_oauth/mediawiki/?requestTokenAndRedirect=true'
    + '&state=' + OAuth._stateParam(loginStyle, credentialToken);

  var loginUrl = Meteor.absoluteUrl(loginPath);

  OAuth.launchLogin({
    loginService: "mediawiki",
    loginStyle: loginStyle,
    loginUrl: loginUrl,
    credentialRequestCompleteCallback: credentialRequestCompleteCallback,
    credentialToken: credentialToken
  });
};
