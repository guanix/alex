var Fiber = Npm.require('fibers');

RoutePolicy.declare('/_mediawiki', 'network');

WebApp.connectHandlers.use(function (req, res, next) {
	Fiber(function () {
		middleware(req, res, next);
	}).run();
});

var splitPath = function (req) {
  // req.url will be "/_oauth/<service name>" with an optional "?close".
  var i = req.url.indexOf('?');
  var barePath;
  if (i === -1)
    barePath = req.url;
  else
    barePath = req.url.substring(0, i);
  var splitPath = barePath.split('/');
  return splitPath[1];
};

var middleware = function (req, res, next) {
	try {
		if (splitPath(req) != '_mediawiki') {
			next();
			return;
		}

		// We can now run the request handler!
		mediawikiRequestHandler(req.query, res);
	} catch (err) {
		try {
			OAuth._endOfLoginResponse(res, {
				query: req.query,
				loginStyle: 'popup',
				error: err
			});
		} catch (err) {
			Log.warn("Error generating end of login response\n" +
					 (err && (err.stack || err.message)));
		}
	}
};

var mediawikiRetrieveRequestToken = function (key) {
  check(key, String);

  var pendingRequestToken = OAuth._pendingRequestTokens.findOne({ requestToken: key });
  if (pendingRequestToken) {
    OAuth._pendingRequestTokens.remove({ _id: pendingRequestToken._id });
    return {
      requestToken: OAuth.openSecret(pendingRequestToken.requestToken),
      requestTokenSecret: OAuth.openSecret(
        pendingRequestToken.requestTokenSecret),
      obj: pendingRequestToken
    };
  } else {
    return undefined;
  }
};

var mediawikiPrepareAccessToken = function (query, requestTokenSecret) {
};

var mediawikiRequestHandler = function (query, res) {
  var config = ServiceConfiguration.configurations.findOne({service: 'mediawiki'});

  if (!config) {
    throw new ServiceConfiguration.ConfigError(service.serviceName);
  }

  // Get request token info
  var requestTokenInfo = mediawikiRetrieveRequestToken(query.oauth_token);

  if (!requestTokenInfo) {
  	throw new Error("Unable to retrieve request token");
  }

  // it's a mess that we can't get back the service object from OAuth
  var urls = {
  	authenticate: config.wikiUrl + '?title=' +
    	encodeURIComponent("Special:OAuth/authorize") +
    	'&oauth_consumer_key=' + config.clientId,
    accessToken: config.wikiUrl + '?title=' +
    	encodeURIComponent("Special:OAuth/token") +
    	'&oauth_consumer_key=' + config.clientId,
    identify: config.wikiUrl + '?title=' +
    	encodeURIComponent("Special:OAuth/identify") +
    	'&oauth_consumer_key=' + config.clientId
  };

  var oauthBinding = new OAuth1Binding(config, urls);
  oauthBinding.prepareAccessToken(query, requestTokenInfo.requestTokenSecret);

  if (!oauthBinding.accessToken || !oauthBinding.accessTokenSecret) {
  	throw new Error("Unable to retrieve access token");
  }

  var headers = oauthBinding._buildHeader({oauth_token: oauthBinding.accessToken});
  var response = oauthBinding._call('GET', urls.identify, headers);

  if (response.data && response.data.error) {
  	throw new Error("MediaWiki identify error: " + response.data.error);
  }

  if (!response.content) {
  	throw new Error("Could not get JWT from MediaWiki");
  }

  var jwt = Meteor.npmRequire('jwt-simple');
  var decoded = jwt.decode(response.content, config.secret);

  if (!decoded) {
  	throw new Error("Could not decode JWT from MediaWiki");
  }

  if (!decoded.username || !decoded.sub) {
  	throw new Error("Information missing from JWT");
  }

  if (decoded.groups.indexOf('hmmember') == -1) {
    throw new Error("Not a HM member");
  }

  OAuth._storePendingCredential(requestTokenInfo.obj.key, {
    serviceName: 'mediawiki',
    serviceData: decoded,
    options: {},
    accessToken: oauthBinding.accessToken
  }, oauthBinding.accessTokenSecret);

  console.log(OAuth._pendingCredentials.findOne({key: requestTokenInfo.obj.key}));

  // Code from _renderOauthResults
  var details = {
    query: query,
    loginStyle: 'popup'
  };

  if (query.error) {
    details.error = query.error;
  } else {
    details.credentials = {
      token: oauthBinding.accessToken,
      secret: oauthBinding.accessTokenSecret,
      jwt: decoded
    };
  }

  OAuth._endOfLoginResponse(res, details);
}
