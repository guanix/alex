Package.describe({
  name: 'guan:accounts-mediawiki',
  summary: ' /* Fill me in! */ ',
  version: '1.0.0'
});

Package.onUse(function(api) {
  api.use('accounts-base', ['client', 'server']);
  api.imply('accounts-base', ['client', 'server']);
  api.use('accounts-oauth', ['client', 'server']);
  api.use('guan:mediawiki', ['client', 'server']);
  api.use('webapp', 'server');
  api.use('routepolicy', 'server');
  api.use('service-configuration', ['client', 'server']);
  api.use('meteorhacks:npm', ['server']);
  api.use('outatime:jwt-simple', ['server']);

  api.addFiles('accounts-mediawiki.js');
  api.addFiles('accounts-mediawiki_server.js', ['server']);
});
